module core

go 1.16

require (
	github.com/apache/rocketmq-client-go/v2 v2.1.0
	github.com/elastic/go-elasticsearch/v8 v8.0.0-20210506154932-f741c073f324
	github.com/tidwall/gjson v1.2.1
)
