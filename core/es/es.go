/*
 * @Author: thepoy
 * @Email: thepoy@163.com
 * @File Name: es.go
 * @Created: 2021-05-07 21:08:32
 * @Modified: 2021-05-08 13:05:19
 */

package es

import "encoding/json"

// Document 文档
type Document struct {
	ID          string `json:"id,omitempty"`
	Title       string `json:"title,omitempty"`
	Content     string `json:"content,omitempty"`
	CreatedDate string `json:"created_date,omitempty"`
}

// String 将文档以序列化为字符串
func (d Document) String() string {
	j, err := json.Marshal(d)
	if err != nil {
		panic(err)
	}

	return string(j)
}
