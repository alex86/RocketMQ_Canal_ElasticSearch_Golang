/*
 * @Author: thepoy
 * @Email: thepoy@163.com
 * @File Name: main.go
 * @Created: 2021-05-06 20:07:27
 * @Modified: 2021-05-08 15:35:32
 */

package main

import (
	"context"
	"core/rocketmq"
	"fmt"
	"time"
)

func main() {
	ctx := context.Background()
	ctx, cancle := context.WithCancel(ctx)

	go func(ctx context.Context) {
		err := rocketmq.Consume(ctx)
		if err != nil {
			fmt.Println(err)
		}
	}(ctx)

	// 模拟阻塞，到时间就调用 cancle() 退出 Consume
	time.Sleep(10 * time.Minute)
	cancle()

	// 休息 2 秒再退出程序，不然可能 consumer 还没有关闭就退出整个程序了
	// 等待 consumer 关闭还有别的方法完成，这里使用最简单的 sleep
	time.Sleep(2 * time.Second)
}
